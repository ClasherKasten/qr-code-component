module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
			fontFamily: {
				"qc-outfit": ["Outfit"]
			},
			colors: {
				"qcc-white": "hsl(0, 0%, 100%)",
				"qcc-light-gray": "hsl(212, 45%, 89%)",
				"qcc-grayish-blue": "hsl(220, 15%, 55%)",
				"qcc-dark-blue": "hsl(218, 44%, 22%)"
			}
		},
  },
  plugins: [],
}